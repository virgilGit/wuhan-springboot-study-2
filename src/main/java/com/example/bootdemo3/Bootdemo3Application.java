package com.example.bootdemo3;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// com.example.bootdemo3.mapper
@SpringBootApplication
@MapperScan("com.example.bootdemo3.mapper")
public class Bootdemo3Application {

    public static void main(String[] args) {
        SpringApplication.run(Bootdemo3Application.class, args);
    }

}
