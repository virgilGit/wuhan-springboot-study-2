package com.example.bootdemo3.util;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RsaUtils {

    public static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALCAAOkfeMZE2jkr\n" +
            "hODujI+8T213fAv+OoSJRDG+gkSbiqDaHloa+hl1pJzrPWG/dAg/2FHoLiWwlgUm\n" +
            "L6s7oSK/DS0K1lqY+zNb/EA08LggOvs4E+Bs7fBBJX7t5bvA7CT+slvPbijZusio\n" +
            "VOVC0K7B7MxcedzW/mdwFxecMkpPAgMBAAECgYEAj18rXjNGdCgx7AbB5cVhQNEC\n" +
            "5DUO+t8EweyPDKloQDrpHUiRJziKuGur0Bv2vJQ8uQbDvaUQ92S+OGiQiFPjy2/c\n" +
            "xpKoI7VZM3T0sqIogvNjfKosUr0zG76PDdIC8Szo/fYrLLATNp4z89kYznQ/jJe0\n" +
            "SwiHQIreqB02N6jai4ECQQDhgCwuEzlXMX5XOy+nfM9sqOJ8MSk+Hyv0MqL+1DTT\n" +
            "NMEqCjhB5a+brcueG1OUJYH4IOdxiSNm595vNRl/9AsfAkEAyF82TNnzWtjNg4ND\n" +
            "pYAx5DlJstI1G5U65W5dq2VMwyhrvYfeLOpn4rDcldSCjvWmNC567aklOJ4qscHx\n" +
            "V7oK0QJAUJSXtQNDm0fhaNSKi+RKzv5uqIh8v2kuML4jhsJX+h1A5X/vaITmQtBR\n" +
            "EbecHzVkA0Hm0qh50R0MXFryOMoBawJAZ6YA4742iT7vCGGlY34t0L3lpq/gMvV9\n" +
            "8uKlZ5zOGYHWQcN/pUJYHMaaRJMaOjNOF14XzsqZa3E96pfFbTjakQJAMtROsHzG\n" +
            "lS8IZ8zfSymdHnZ26daz0KCZTIVnXUa1143JUqMzF5muZwNgkCwW38d9sWklyReH\n" +
            "YRmf5Q1Xj9i3Jg==";

    public static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwgADpH3jGRNo5K4Tg7oyPvE9t\n" +
            "d3wL/jqEiUQxvoJEm4qg2h5aGvoZdaSc6z1hv3QIP9hR6C4lsJYFJi+rO6Eivw0t\n" +
            "CtZamPszW/xANPC4IDr7OBPgbO3wQSV+7eW7wOwk/rJbz24o2brIqFTlQtCuwezM\n" +
            "XHnc1v5ncBcXnDJKTwIDAQAB";

    /**
     * 对数据原文使用私钥进行签名
     * @param text 数据原文
     * @return 签名
     */
    public static String sign(String text) {

        try{

            // 准备私钥对象
            PrivateKey privateKey = KeyFactory.getInstance("RSA")
                    .generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(RsaUtils.privateKey)));

            // 生成签名对象
            Signature signature = Signature.getInstance("NONEwithRSA");
            // 签名对象使用哪个私钥进行加密
            signature.initSign(privateKey);
            // 设置需要加密的原文
            signature.update(text.getBytes());

            byte[] sign = signature.sign();

            return Base64.encodeBase64String(sign);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 收到数据的节点，会先使用公钥对密文进行解析，得到的结果和原文进行比较
     * 如果都一致，则验证成功
     * @param text 数据原文
     * @param sign 签名
     * @return 验证成功失败
     */
    public static boolean verify(String text, String sign) {

        try{

            PublicKey publicKeyInstance = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(publicKey)));
            Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initVerify(publicKeyInstance);
            signature.update(text.getBytes());

            // verify方法会自动将原文和签名通过公钥进行匹配
            return signature.verify(Base64.decodeBase64(sign));
        }catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {

        String text = "{\"id\":\"xxxx\", \"name\":\"冰红茶\"}";
        String sign = sign(text);
        System.out.println(sign);
        boolean success = verify("{\"id\":\"xxxx\", \"name\":\"冰红茶\"}", sign);
        System.out.println(success);
    }
}
