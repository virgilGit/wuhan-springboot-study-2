package com.example.bootdemo3.conf;

import com.example.bootdemo3.security.interceptor.AuthorizationInterceptor;
import com.example.bootdemo3.security.interceptor.CookieToSessionInterceptor;
import com.example.bootdemo3.security.interceptor.DefaultSessionAuthenticationInterceptor;
import com.example.bootdemo3.security.interceptor.JwtAuthenticationInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 修饰类的Configuration注解表示，注册到spring容器中，作为配置类来使用
 * 用来配置其他的注册到spring容器的组件
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

    @Autowired
    private DefaultSessionAuthenticationInterceptor sessionAuthenticationInterceptor;

    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;

    @Autowired
    private JwtAuthenticationInterceptor jwtAuthenticationInterceptor;

    @Autowired
    private CookieToSessionInterceptor cookieToSessionInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {

        //TODO CookieToSessionInterceptor拦截范围怎么界定
        registry.addInterceptor(cookieToSessionInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/doc.html")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/asserts/**")
                .excludePathPatterns("/swagger-resources");

        registry.addInterceptor(sessionAuthenticationInterceptor)
                // 设置拦截器的作用地址，作用范围
                .addPathPatterns("/**")
                // 设置拦截器哪些地址不拦截
                .excludePathPatterns("/login")
                .excludePathPatterns("/doc.html")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/asserts/**")
                .excludePathPatterns("/swagger-resources");


        registry.addInterceptor(authorizationInterceptor)
                // 设置拦截器的作用地址，作用范围
                .addPathPatterns("/**")
                // 设置拦截器哪些地址不拦截
                .excludePathPatterns("/login")
                .excludePathPatterns("/doc.html")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/asserts/**")
                .excludePathPatterns("/swagger-resources");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) { // 添加静态资源

        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");

        registry.addResourceHandler("/doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
