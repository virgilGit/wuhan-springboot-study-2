package com.example.bootdemo3.advice;

import com.alibaba.fastjson.JSONObject;
import com.example.bootdemo3.util.RsaUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class SignatureVerifyAdvice {

    @Pointcut("@annotation(com.example.bootdemo3.advice.annotation.Verify)")
    public void verify() {

    }

    @Around("verify()")
    public Object verify(ProceedingJoinPoint joinPoint) throws Throwable {

        // request
        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        // 准备签名
        String sign = request.getHeader("sign");

        // 准备数据正文
        // request.getInputStream只能完整得获取数据一次，也就是说，我把数据原文取出来了
        // 就不能够使用@RequestBody注解，让框架帮助我们去加载controller参数
        int contentLength = request.getContentLength();
        if (contentLength < 0) {
            throw new RuntimeException("数据为空！");
        }
        byte[] buffer = new byte[contentLength];

        for (int i = 0; i < contentLength;) {

            int readLength = request.getInputStream().read(buffer, i, contentLength - i);
            if (readLength == -1) {
                break;
            }
            i += readLength;
        }
        String json = new String(buffer, "UTF-8");

        // 进行验签
        boolean verify = RsaUtils.verify(json, sign);
        if (!verify) {
            throw new RuntimeException("签名不匹配，数据可能被篡改或者伪造！");
        }

        // 代替spring框架，将json转换为指定类型的实体类，赋值到被拦截的方法参数列表中

        // 获取参数列表，获取第一个参数的类型
        Object[] args = joinPoint.getArgs();
        Class<?> clazz = args[0].getClass();

        Object data = JSONObject.parseObject(json, clazz);
        args[0] = data;

        return joinPoint.proceed(args);
    }
}
