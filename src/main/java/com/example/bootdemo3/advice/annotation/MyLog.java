package com.example.bootdemo3.advice.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 元注解
 * Target 设置当前注解类只能够修饰哪些地方
 *  METHOD 方法
 *  PARAMETER 方法中的参数
 *  TYPE 类
 *  FIELD 成员变量
 * Retention 注解的生命周期
 *  RUNTIME 当前注解再程序运行的时候依旧存在
 *  SOURCE 仅仅再源码状态注解才会存在
 * @author admin
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyLog {
    /**
     * 注解类中可以设置变量，
     * 只不过变量需要使用抽象方法的写法来编写
     * @return
     */
    String value();
}
