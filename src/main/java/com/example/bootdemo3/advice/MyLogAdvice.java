package com.example.bootdemo3.advice;

import com.example.bootdemo3.advice.annotation.MyLog;
import com.example.bootdemo3.domain.SysLog;
import com.example.bootdemo3.service.ISysLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Aspect
@Component // 将组件注入到Spring容器中
@Slf4j
public class MyLogAdvice {

    @Autowired
    ISysLogService sysLogService;

    /**
     * 定义切点，表示被@MyLog修饰的方法都会被该切点表达式选中
     */
    @Pointcut("@annotation(com.example.bootdemo3.advice.annotation.MyLog)")
    public void myLog() {

    }

    /**
     * 增强，需要注入的代码，我们可以把这些代码注入到以下几个地方
     * @Before 注入到切点执行之前
     * @AfterReturning 切点正常执行之后
     * @After 切点只要执行完成了，不管有没有报错，都会执行的增强代码
     * @AfterThrowing 切点只要执行报错了，会添加的增强代码
     * @Around 环绕增强，我们可以通过代码手动控制切点方法再什么时候执行
     */
    @Around("myLog()")
    public Object mylogAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        /**
         * 打印调用了什么方法？
         * 什么时候调用的？
         * 什么时候调用结束？
         * 本次调用花了多少时间？
         * 本次调用成功还是失败了？
         */

        // 1. 获取切点方法对象
        SysLog sysLog = new SysLog();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        MyLog annotation = method.getAnnotation(MyLog.class);
        String methodName = annotation.value();

        long startDate = System.currentTimeMillis();
        log.info("调用方法：" + methodName + "，在" + new Date(startDate) + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        Instant startInstant = Instant.ofEpochMilli(startDate);
        sysLog.setStartTime(LocalDateTime.ofInstant(startInstant, ZoneId.systemDefault()))
                .setMethodName(methodName);
        try{

            Object proceed = joinPoint.proceed();
            log.info("方法执行成功！");
            sysLog.setLogStatus(1);
            sysLog.setMsg("方法执行成功！");
            return proceed;
        }catch (Throwable e) {

            log.error("方法执行期间出现了异常：" + e.getMessage(), e);
            sysLog.setLogStatus(0)
                    .setMsg(e.getMessage());
            throw e;
        } finally {
            long endDate = System.currentTimeMillis();
            log.info("方法执行结束，结束时间是：" + new Date(endDate) + "，总共耗时" + (endDate - startDate) + "ms");
            sysLog.setEndTime(
                    LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(endDate), ZoneId.systemDefault()))
                    .setDuringTime(endDate - startDate);

            sysLogService.save(sysLog);
        }

    }
}
