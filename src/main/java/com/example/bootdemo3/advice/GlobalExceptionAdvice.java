package com.example.bootdemo3.advice;

import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.exception.AuthenticationException;
import com.example.bootdemo3.validate.MyValidateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * RestController 用来和客户端进行连接，对请求做出响应
 * Advice 增强，代码增强，可以将一些代码统一地注入到某些代码中；
 * RestControllerAdvice 能够拦截所有地Controller方法，当这些方法执行地过程中报错
 * 这些错误就会被GlobalExceptionAdvice拦截到，并且做出相应地处理
 * 一般处理这些错误信息地时候，使用日志打印错误；将错误翻译翻译，告诉客户端系统出现了哪些错误
 *
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    /**
     * 构造一个日志对象
     * 能够控制日志的打印
     */
    private final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionAdvice.class);

    @ExceptionHandler(ArithmeticException.class)
    public RespBean handleArithmeticException(ArithmeticException e) {

        // 打印日志，打印异常
        LOGGER.error("出现了算术错误：" + e.getMessage(), e);
        // 将异常格式化，通知到客户端
        return RespBean.error(501, "出现了算术错误：" + e.getMessage());
    }

    @ExceptionHandler(MyValidateException.class)
    public RespBean handleMyValidateException(MyValidateException e) {

        LOGGER.error(e.getMessage(), e);

        return RespBean.error(e.getCode(), "数据校验异常：" + e.getMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    public RespBean handleAuthenticationException(AuthenticationException e) {

        LOGGER.error(e.getMessage(), e);

        return RespBean.error(510, e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public RespBean handleException(Exception e) {

        LOGGER.error(e.getMessage(), e);

        return RespBean.error(500, "未知异常：" + e.getMessage());
    }
}
