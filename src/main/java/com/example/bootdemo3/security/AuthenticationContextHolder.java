package com.example.bootdemo3.security;

import com.example.bootdemo3.domain.SysUser;
import org.springframework.stereotype.Component;

/**
 * Spring Security
 * 会在内部维护一个Map<请求, 用户信息>
 * Tomcat 服务器作为一个web容器，每当一个请求进来了，tomcat会从自己维护得线程池中找一个空闲得线程
 * 使用该线程为本次请求提供服务，当请求结束得时候，该线程又会回到线程池处于空闲状态；
 * 我们每一个请求在执行得时候，其实就是一个线程在进行工作
 * 因此我们维护得Map它得key值就是一个Thread类型（线程）
 * Map<Thread, SysUser>
 */
@Component
public class AuthenticationContextHolder {

    /**
     * 其内部相当于维护了一个Map<Thread, SysUser>
     */
    private final ThreadLocal<SysUser> AUTHENTICATION_CONTEXT = new ThreadLocal<>();

    public SysUser getAuth() {

        // map#get(Thread.currentThread())
        return AUTHENTICATION_CONTEXT.get();
    }

    public void addSysUser(SysUser s) {
        AUTHENTICATION_CONTEXT.set(s);
    }

    public void remove() {
        AUTHENTICATION_CONTEXT.remove();
    }
}
