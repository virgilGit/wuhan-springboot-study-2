package com.example.bootdemo3.security.interceptor;

import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.exception.AuthenticationException;
import com.example.bootdemo3.mapper.SysUserMapper;
import com.example.bootdemo3.security.AuthenticationContextHolder;
import com.example.bootdemo3.service.ISysUserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 流程：
 * 1. 从请求头中获取token，jwt
 * 2. 解析jwt
 * 3. 如果解析成功，读取到id，根据id查询用户信息
 * 4. 将查询到得用户信息放到ContextHolder
 * 5. 放行拦截器，访问受保护得资源
 * 6. 请求结束得时候，清空该请求临时得用户信息
 */
@Component
public class JwtAuthenticationInterceptor implements HandlerInterceptor {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.type}")
    private String tokenType;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private AuthenticationContextHolder contextHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String header = request.getHeader(tokenHeader);
        if (!StringUtils.isEmpty(header)) {
            String jwt = header.substring(tokenType.length());

            // 解析jwt
            Claims body = Jwts.parser()
                    .setSigningKey(secret.getBytes())
                    .parseClaimsJws(jwt)
                    .getBody();
            String userId = body.getSubject();

            // TODO 可以放到缓存redis
            SysUser sysUser = sysUserMapper.findById(userId);

            if (sysUser == null) {
                throw new AuthenticationException("个人信息解析异常，可能被管理员删除");
            }

            contextHolder.addSysUser(sysUser);
        }

        // 验证用户是否登录
        if (contextHolder.getAuth() != null) {
            return true;
        } else {
            throw new AuthenticationException("token失效，请重新登录");
        }
    }
}
