package com.example.bootdemo3.security.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class CookieToSessionInterceptor implements HandlerInterceptor {

    @Value("${my.session.cookie_key}")
    private String cookieKey;

    @Value("${my.session.max-timeout}")
    private Integer maxTimeOut;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        /**
         * 该拦截器只是确保每一个请求都会有一个对应的session，本身并不去做认证、授权等等判断
         * 因此，无论session是否存在，执行完逻辑之后，都会放行到下一个interceptor；
         *
         * 从请求中读取cookie，找到MYSESSIONID的cookie
         * 1. 验证mySessionId是否存在
         *  a. 如果存在的话，验证在redis中是否存在
         *      1. 在redis中存在
         *          刷新当前的session数据在redis中的有效期
         *      2. 在redis中不存在
         *          执行b的代码
         *  b. 不过不存在mySessionId：
         *      1. 生成一个MYSESSIONID
         *      2. 使用reponse的setCookie设置到cookie中传递给客户端
         *      3. 在redis中声明一个新的会话空间
         *
         */
        String mySessionId = "";
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {

            if (cookies[i].getName().equals(cookieKey)) {
                mySessionId = cookies[i].getValue();
            }
        }

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        if (!StringUtils.isEmpty(mySessionId)) {
            // 在redis中存在
            Object session = operations.get(cookieKey + "_" + mySessionId);
            if (session != null) {
                // 说明用户的会话信息有效，刷新当前会话
                redisTemplate.expire(cookieKey + "_" + mySessionId, maxTimeOut, TimeUnit.MINUTES);
                return true;
            }
        }

        // 如果会话无效：mySessionId都不存在；mySessionId存在但是过期了；
        mySessionId = UUID.randomUUID().toString().replace("-", "");

        // 将sessionId发送给客户端
        Cookie cookie = new Cookie(cookieKey, mySessionId);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);

        // 将sessionId保存在服务器
        operations.set(cookieKey + "_" + mySessionId, new HashMap<String, Object>(), maxTimeOut, TimeUnit.MINUTES);

        return true;
    }
}
