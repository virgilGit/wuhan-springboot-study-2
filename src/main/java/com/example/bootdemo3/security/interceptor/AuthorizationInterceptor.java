package com.example.bootdemo3.security.interceptor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.bootdemo3.domain.SysPermission;
import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.security.AuthenticationContextHolder;
import com.example.bootdemo3.security.util.DistributedSessionUtils;
import com.example.bootdemo3.service.ISysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component
public class AuthorizationInterceptor implements HandlerInterceptor {

    @Autowired
    private ISysPermissionService permissionService;

    @Autowired
    private AuthenticationContextHolder contextHolder;

    @Autowired
    private DistributedSessionUtils distributedSessionUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 2. 从request请求中获取请求地址 HttpServletRequest#getRequestURI -> uri
        String requestURI = request.getRequestURI();
        String requestMethod = request.getMethod().toUpperCase();

        // 3. 拿着uri到数据库中查询对应的SysPermission信息
        SysPermission neededPerm = permissionService.getOne(new QueryWrapper<SysPermission>()
                .eq("perm_url", requestURI)
                .eq("perm_method", requestMethod)
        );
        if (neededPerm == null) {
            // 该资源不需要限制权限，登录的用户都可以访问
            return true;
        }

        // 4. 从用户的参数中找到用户具备哪些权限
        // 从jwt解析到的ContextHolder中获取用户信息
//        SysUser auth = contextHolder.getAuth();

        // 从session中获取用户信息
        // SysUser auth = (SysUser) request.getSession().getAttribute("auth");

        // 从分布式session中获取用户信息
        Map<String, Object> session = (Map<String, Object>) distributedSessionUtils.getDistributedSession(request);
        SysUser auth = (SysUser) session.get("auth");

        List<SysPermission> allowedPerms = auth.getPerms();

        //5. 依次遍历用户的权限，比较和uri需要的权限是否一致，如果有一致的匹配，就认定用户有权限来访问当前的资源
        for (SysPermission allowedPerm : allowedPerms) {

            if (allowedPerm.equals(neededPerm)) {
                return true;
            }
        }

        throw new RuntimeException("授权不通过，请联系管理员索要相应的权限！");
    }
}
