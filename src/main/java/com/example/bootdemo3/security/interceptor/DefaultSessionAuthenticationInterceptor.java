package com.example.bootdemo3.security.interceptor;

import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.exception.AuthenticationException;
import com.example.bootdemo3.security.util.DistributedSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 将当前组件单例化，并且交给spring容器统一管理和调配
 * @author admin
 */
@Component
public class DefaultSessionAuthenticationInterceptor implements HandlerInterceptor {

    @Autowired
    private DistributedSessionUtils distributedSessionUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // @Before

        // 读取request中的JSESSIONID 如果有的话，拿着这个JSESSIONID到服务器中找到其对应的HttpSession对象
        // 如果读取不到JSESSIONID或者该JSESSIONID查不到对应的HttpSession对象
        // 由服务器一式两份颁发令牌：
        // 生成随机的JSESSIONID
        // 一份 通过Set-Cookie响应头发送给客户端JSESSIONID
        // 一份 使用JSESSIONID作为key，已经生成一个新的HttpSession对象作为value放到服务器的map表

        // 基于session的认证体系
        // HttpSession对象相当于一个Map也可以存放键值对
//        HttpSession session = request.getSession();
        // 调用该用户会话对象中，之前登录时保存的用户信息
//        Object auth = session.getAttribute("auth");

        // 基于分布式session的认证体系
        SysUser auth = distributedSessionUtils.getDistributedSessionAttr("auth", request);

        if (auth == null) {
            // 用户根本登录过
            throw new AuthenticationException("登录信息过期，请重新登录！");
        } else {
            // 将请求传递下去，传递到下一个interceptor，如果没有interceptor，就会传递到对应的controller方法上
            return true;
        }
    }

}
