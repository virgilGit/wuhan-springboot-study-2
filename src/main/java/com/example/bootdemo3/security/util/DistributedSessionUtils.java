package com.example.bootdemo3.security.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class DistributedSessionUtils {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Value("${my.session.cookie_key}")
    private String cookieKey;

    @Value("${my.session.max-timeout}")
    private Integer maxTimeOut;

    public Object getDistributedSession(HttpServletRequest request) {


        String mySessionId = getMySessionId(request);

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        return  operations.get(cookieKey + "_" + mySessionId);
    }

    public Object getDistributedSession(String mySessionId) {

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        return  operations.get(cookieKey + "_" + mySessionId);
    }

    public void setDistributedSessionAttr(String key, Object value, HttpServletRequest request) {

        String mySessionId = getMySessionId(request);

        String redisKey = cookieKey + "_" + mySessionId;

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();

        Map<String, Object> session = (Map<String, Object>) operations.get(redisKey);

        if (session == null) {
            return;
        }
        session.put(key, value);

        operations.set(redisKey, session, maxTimeOut, TimeUnit.MINUTES);
    }

    public <T> T getDistributedSessionAttr(String key, HttpServletRequest request) {

        Map<String, Object> session = (Map<String, Object>) getDistributedSession(request);

        if (session == null) {
            return null;
        }

        Object value = session.get(key);

        return (T) value;
    }

    public String getMySessionId(HttpServletRequest request) {

        String mySessionId = "";
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {

            if (cookies[i].getName().equals(cookieKey)) {
                mySessionId = cookies[i].getValue();
            }
        }

        return mySessionId;
    }
}
