package com.example.bootdemo3.domain;

import lombok.Data;

import java.util.List;

@Data
public class SysRole {

    private Integer roleId;

    private String roleName;

    private String roleDesc;

    private List<SysPermission> permissions;
}
