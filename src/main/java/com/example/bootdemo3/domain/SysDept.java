package com.example.bootdemo3.domain;

import lombok.Data;

import java.util.List;

@Data
public class SysDept {

    private Integer deptId;

    private String deptName;

    private String deptDesc;

    private List<SysUser> userList;
}
