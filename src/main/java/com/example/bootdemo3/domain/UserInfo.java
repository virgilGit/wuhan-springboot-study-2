package com.example.bootdemo3.domain;

import lombok.Data;

@Data
public class UserInfo {

    private Integer id;

    private String name;

    private String gender;

    private String phone;
}
