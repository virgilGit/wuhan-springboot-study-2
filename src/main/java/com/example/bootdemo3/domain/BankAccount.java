package com.example.bootdemo3.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BankAccount {

    private Integer accountId;

    private String accountName;

    private BigDecimal accountAmount;

    private Date accountStartDate;
}
