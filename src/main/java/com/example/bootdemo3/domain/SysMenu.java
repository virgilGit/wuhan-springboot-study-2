package com.example.bootdemo3.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SysMenu {

    private Integer menuId;

    private String menuName;

    private String menuDesc;

    private Integer parentId;

    @ApiModelProperty("当前菜单所拥有的子菜单")
    private List<SysMenu> children;
}
