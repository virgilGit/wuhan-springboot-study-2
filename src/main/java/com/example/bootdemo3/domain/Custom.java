package com.example.bootdemo3.domain;

import lombok.Data;

@Data
public class Custom {

    private String firstName;

    private String lastName;
}
