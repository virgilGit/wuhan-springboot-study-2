package com.example.bootdemo3.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser implements Serializable {

    @ApiModelProperty("用户的主键")
    @TableId(type = IdType.AUTO) // 设置当前的字段为主键，并且是自增长的主键
    private Integer userId;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("邮箱地址")
    private String email;

    @ApiModelProperty("性别：1 男性，0女性")
    private Integer gender;

    @TableField(exist = false) // 在自动生成sql语句的时候，会忽略这些字段
    @ApiModelProperty(hidden = true)
    private BankAccount account;

    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private List<SysRole> roles;

    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private List<SysPermission> perms;

    // Alt + Insert

    public SysUser(Integer userId, String username, String password, String email) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.email = email;
    }
}
