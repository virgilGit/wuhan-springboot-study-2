package com.example.bootdemo3.domain;

import lombok.Data;

@Data
public class Goods {

    private String id;

    private String name;
}
