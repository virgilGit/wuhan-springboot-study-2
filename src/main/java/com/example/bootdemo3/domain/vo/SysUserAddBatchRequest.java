package com.example.bootdemo3.domain.vo;

import com.example.bootdemo3.domain.SysUser;
import lombok.Data;

@Data
public class SysUserAddBatchRequest {

    SysUser[] data;
}
