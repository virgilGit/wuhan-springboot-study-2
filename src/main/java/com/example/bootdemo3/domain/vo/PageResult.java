package com.example.bootdemo3.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PageResult<T> {

    private Integer total;

    private Integer totalPage;

    private Integer currentPage;

    private Integer size;

    private List<T> data;
}
