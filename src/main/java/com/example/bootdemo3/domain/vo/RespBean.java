package com.example.bootdemo3.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RespBean {

    private Integer code;

    private String msg;

    private Object data;

    public static RespBean success(String msg, Object data) {

        return new RespBean().setCode(200)
                .setMsg(msg)
                .setData(data);
    }

    public static RespBean success(String msg) {

        return success(msg, null);
    }

    public static RespBean success(Object data) {

        return success("请求成功！", data);
    }

    public static RespBean error(Integer code, String msg) {

        return new RespBean().setCode(code)
                .setMsg(msg);
    }
}
