package com.example.bootdemo3.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("sys_permission")
@EqualsAndHashCode(of = "permName")
public class SysPermission {

    @TableId(type = IdType.AUTO)
    private Integer permId;

    private String permName;

    private String permDesc;

    private String permUrl;

    private String permMethod;
}
