package com.example.bootdemo3.validate;


import com.example.bootdemo3.validate.annotation.MyBetween;
import com.example.bootdemo3.validate.annotation.MyRequired;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Aspect
@Component
public class ValidateAdvice {

    @Pointcut("@annotation(com.example.bootdemo3.validate.annotation.MyValidated)")
    public void validatePointcut() {

    }

    @Before("validatePointcut()")
    public void validateData(JoinPoint joinPoint) {

        /**
         * Method
         * 获取到该方法所有的参数 args
         * 遍历这些参数，检查是否被@MyRequired注解修饰
         *  如果是，则会验证该参数是否不为空；如果不符合验证条件，将错误信息搜集起来，最后抛出异常
         */

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        // 获取实参列表 ['小红', 18]
        Object[] args = joinPoint.getArgs();

        // 获取入参的数组 [String name, Integer age]
        Parameter[] parameters = method.getParameters();

        // flag，用来记录是否由校验报错的情况
        boolean validateSuccess = true;

        // 用来搜集所有的错误信息
        StringBuilder errorMsg = new StringBuilder();

        for (int i = 0; i < parameters.length; i++) {

            if (parameters[i].isAnnotationPresent(MyRequired.class)) {
                // 将该参数对应的值，做一个数据校验
                // 自己定义校验规则
                Object value = args[i];
                boolean empty = StringUtils.isEmpty(value);
                if (empty) {
                    validateSuccess = false;
                    errorMsg.append("参数" + parameters[i].getName() + "校验非空时出错！\n");
                }
            }
            if (parameters[i].isAnnotationPresent(MyBetween.class)) {

                //
            }
        }

        if (!validateSuccess) {
            throw new MyValidateException(550, errorMsg.toString());
        }
    }
}
