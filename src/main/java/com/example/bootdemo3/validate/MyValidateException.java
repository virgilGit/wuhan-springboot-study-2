package com.example.bootdemo3.validate;

public class MyValidateException extends RuntimeException {

    private Integer code;

    public MyValidateException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
