package com.example.bootdemo3.mapper;

import com.example.bootdemo3.domain.BankAccount;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface BankAccountMapper {

    @Select("select * from bank_account where account_id = #{accountId}")
    BankAccount findByAccountId(@Param("accountId") Integer accountId);
}
