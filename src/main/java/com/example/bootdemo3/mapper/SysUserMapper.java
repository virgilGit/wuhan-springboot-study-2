package com.example.bootdemo3.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bootdemo3.domain.SysUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 用来定义访问数据库方法的接口
 * 定义操作数据库的方法：增删改查
 *
 * 标记当前接口是一个用来操作数据库的对象，
 * 这个对象作为一个单例对象也交给spring boot管理
 * 增删改查
 * find
 * insert
 * update
 * delete
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    SysUser findById(@Param("userId") String userId);

    List<SysUser> findAllWithRoles2();

    @Select("select * from sys_user where dept_id = #{deptId}")
    List<SysUser> findByDeptId(@Param("deptId") Integer deptId);

    List<SysUser> findAllWithRoles();

    int insertBatch(@Param("sysUsers") SysUser[] sysUsers);

    List<SysUser> findAllWithAccount();

    List<SysUser> findAllWithAccount2();

    List<SysUser> findAll();

    int insertSysUser(SysUser sysUser);

    int insertSysUser2(Map<String, Object> params);

    int updateSysUser(SysUser sysUser);

    @Delete("delete from sys_user where user_id = #{userId}")
    int deleteById(@Param("userId") int id);

    SysUser findByUsernameAndPassword(@Param("username") String username,
                                      @Param("password") String password);

    List<SysUser> findByConditions(SysUser sysUser);

    @Select("select count(*)\n" +
            "from bootdemo3.sys_user")
    int findAllCount();

    @Select("select count(*) from sys_user where gender = #{gender}")
    int findAllCountByGender(@Param("gender") Integer gender);

    List<Map<String, Object>> findGenderCount();

    List<SysUser> findLikeUsername(@Param("username") String username);

    List<SysUser> findLikeUsername2(@Param("username") String username);

    List<SysUser> findLikeUsername3(@Param("username") String username);

    /**
     *         delete from sys_user where user_id
     *
     *         <foreach collection="userIds" item="userId" open="in ("
     *                  close=")" separator=",">
     *             #{userId}
     *         </foreach>
     * @param userIds
     * @return
     */
    int deleteByIds(@Param("userIds") Integer[] userIds);

    @Select("select * from sys_user limit #{index}, #{size}")
    List<SysUser> findAllPage(Integer index, Integer size);
}
