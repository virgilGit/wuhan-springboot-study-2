package com.example.bootdemo3.mapper;

import com.example.bootdemo3.domain.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMapper {

    List<SysRole> findByUserId(@Param("userId") Integer userId);
}
