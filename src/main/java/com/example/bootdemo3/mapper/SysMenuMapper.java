package com.example.bootdemo3.mapper;

import com.example.bootdemo3.domain.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuMapper {

    List<SysMenu> findByParentId(@Param("parentId") Integer parentId);
}
