package com.example.bootdemo3.mapper;

import com.example.bootdemo3.domain.SysDept;
import com.example.bootdemo3.domain.SysUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysDeptMapper {

    List<SysDept> findAllWithUser();

    List<SysDept> findAllWithUser2();
}
