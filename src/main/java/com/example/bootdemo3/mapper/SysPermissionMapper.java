package com.example.bootdemo3.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bootdemo3.domain.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermission> findByRoleId(@Param("roleId") Integer roleId);
}
