package com.example.bootdemo3.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.bootdemo3.advice.annotation.MyLog;
import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.domain.vo.PageResult;
import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.domain.vo.SysUserAddBatchRequest;
import com.example.bootdemo3.service.ISysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "用户管理")
public class SysUserController {

    @Autowired
    private ISysUserService sysUserService;

    @GetMapping("/sysUser/roles/perms")
    @ApiOperation("查看所有用户信息，并且加载角色、权限")
    public RespBean getUserWithRoleAnsPerm() {

        List<SysUser> users = sysUserService.findAllWithRoleAndPerm();
        return RespBean.success(users);
    }

    /**
     * Ctrl + D 整行复制
     * Ctrl + Y 整行删除
     * @param sysUser
     * @param currentPage
     * @param size
     * @return
     */
    @GetMapping("/sysUser/list")
    @ApiOperation("按照多条件进行查询")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "currentPage", value = "当前页", defaultValue = "1", required = true),
//            @ApiImplicitParam(name = "size", value = "页面大小", defaultValue = "5", required = true),
//            @ApiImplicitParam(name = "userId", value = "用户id", required = false),
//            @ApiImplicitParam(name = "username", value = "用户名", required = false),
//            @ApiImplicitParam(name = "password", value = "密码", required = false),
//            @ApiImplicitParam(name = "email", value = "邮箱", required = false),
//            @ApiImplicitParam(name = "gender", value = "性别：1 男性 0 女性", required = false),
//
//    })
    public RespBean getListByConditons(SysUser sysUser, Integer currentPage, Integer size) {

        PageHelper.startPage(currentPage, size);
        List<SysUser> users = sysUserService.getByConditions(sysUser);
        PageInfo<SysUser> pageInfo = new PageInfo<>(users);

        return RespBean.success(pageInfo);
    }

    @GetMapping("/sysUser")
    @ApiOperation("获取所有用户")
    @MyLog("获取所有用户方法")
    public RespBean getList(Integer currentPage, Integer size) {

        // 拦截下一次sql语句，制作一个分页的功能
        // 将拦截的sql补充limit语句
        // PageHelper自动会进行total totalPage的统计的工作
        PageHelper.startPage(currentPage, size);
        List<SysUser> list = sysUserService.getAll();
        PageInfo<SysUser> pageInfo = new PageInfo<>(list);

        return RespBean.success(pageInfo);
    }

    @PostMapping("/sysUser")
    @ApiOperation("新增用户")
    @MyLog("新增用户方法")
    public RespBean addUser(@RequestBody SysUser sysUser) {

        // 1. 收集客户端传递过来的数据
        // 数据校验
        // 2. 将数据封装下，传递给sercie，调用service，执行业务逻辑
        int i = sysUserService.addSysUser(sysUser);
        // 3. 获取service返回的结果，封装后，响应给客户端

        return RespBean.success("成功新增了" + i + "条！");
    }

    @PutMapping("/sysUser")
    @ApiOperation("修改用户")
    public RespBean editUser(@RequestBody SysUser sysUser) {

        int i = sysUserService.editSysUser(sysUser);

        return RespBean.success("成功修改了" + i + "条！");
    }

    @DeleteMapping("/sysUser/{id}")
    @ApiOperation("删除用户")
    public RespBean removeById(@PathVariable("id") Integer id) {

        int i = sysUserService.removeById(id);
        return RespBean.success("成功删除了" + i + "条！");
    }

    /**
     * /xx/xxx?ids=1&ids=2
     * @param ids
     * @return
     */
    @DeleteMapping("/sysUser/batch")
    @ApiOperation("批量按照id数组删除用户数据")
    public RespBean removeByIds(Integer[] ids) {

        int result = sysUserService.removeByIds(ids);

        return RespBean.success("成功删除了" + result + "条！");
    }

    @GetMapping("/sysUser/page")
    @ApiOperation("分页查询用户信息")
    public RespBean pageSysUser(@RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                                @RequestParam(value = "size", defaultValue = "5") Integer size) {

        // 封装数据
        // 调用servcie方法，执行业务逻辑
        PageResult<SysUser> pageResult = sysUserService.getAllPage(currentPage, size);
        // 返回servcie的响应，封装为RespBean响应客户端

        return RespBean.success(pageResult);
    }

    /**
     * {
     *     "data": [
     *      {},{},{}
     *     ]
     * }
     * @param data
     * @return
     */
    @PostMapping("/sysUser/batch")
    @ApiOperation("批量新增用户数据")
    public RespBean saveBatch(@RequestBody JSONObject data) {

        JSONArray jsonArray = data.getJSONArray("data");
        // JSONArray -> List<Map<>>
        // JSONArray#toJavaList -> 遍历List<Map> ->把每一个map都手动转换为SysUser -> 收集 List<SysUser>
        //  LIst<map> {map1 map2}
        // map1 -> map () -> sysUser -> toArray
        //                   map2 -> map() -> sysUser -> toArray
        SysUser[] userArr = jsonArray.stream()
                .map(x -> {
                    Map<String, Object> map = (Map<String, Object>) x;
                    return new SysUser()
                            .setUsername((String) map.get("username"))
                            .setPassword((String) map.get("password"))
                            .setGender((Integer) map.get("gender"))
                            .setEmail((String) map.get("email"));
                })
                .toArray(SysUser[]::new);

//        List<SysUser> sysUsers = jsonArray.toJavaList(SysUser.class);
//        SysUser[] sysUsers1 = sysUsers.toArray(new SysUser[]{});

        int result = sysUserService.addBatch(userArr);

        return RespBean.success("成功插入" + result + "条！");
    }
}
