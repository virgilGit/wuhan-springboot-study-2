package com.example.bootdemo3.controller;

import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.exception.AuthenticationException;
import com.example.bootdemo3.security.util.DistributedSessionUtils;
import com.example.bootdemo3.service.ISysUserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AuthenticationController {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private DistributedSessionUtils distributedSessionUtils;

    @Value("${jwt.secret}")
    private String secret;

    @PostMapping("/login")
    public RespBean login(@RequestBody SysUser sysUser, HttpServletRequest request) {

        SysUser user = sysUserService.getByAuth(sysUser);
        if (user != null) {

            // 基于session的认证，登录成功时，将数据放到session中
//            request.getSession().setAttribute("auth", user);
            // 基于分布式session的认证
            distributedSessionUtils.setDistributedSessionAttr("auth", user, request);

            // 基于jwt的认证，登录成功时，将数据解析为jwt发送给客户端
//            String jwt = Jwts.builder()
//                    .setSubject(String.valueOf(user.getUserId()))
//                    .setId(UUID.randomUUID().toString())
//                    .setExpiration(new Date(System.currentTimeMillis() + 30 * 60 * 1000))
//                    .signWith(SignatureAlgorithm.HS256, secret.getBytes()).compact();

//            return RespBean.success("登录成功！", jwt);
            return RespBean.success("登录成功！");
        }
        throw new AuthenticationException("用户名或者密码不正确！");
    }
}
