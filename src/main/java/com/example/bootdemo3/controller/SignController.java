package com.example.bootdemo3.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.bootdemo3.advice.annotation.Verify;
import com.example.bootdemo3.domain.Custom;
import com.example.bootdemo3.domain.Goods;
import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.util.RsaUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sign")
public class SignController {

    @PostMapping("/custom")
    @Verify
    public RespBean verifyCustom(Custom custom) {

        return RespBean.success("验证签名成功！", custom);
    }

    @PostMapping("/goods")
    @Verify
    public RespBean verifyGoods(Goods goods) {

        return RespBean.success("验证签名成功！" + goods);
    }
}
