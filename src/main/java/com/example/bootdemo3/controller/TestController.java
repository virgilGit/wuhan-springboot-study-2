package com.example.bootdemo3.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.bootdemo3.domain.UserInfo;
import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.exception.AuthenticationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/1") // /test/1
    public String test1() {

        throw new RuntimeException("运行时的测试异常！");
    }

    /**
     * @param result 数据的主键
     * @return 删除的结果
     */
    @DeleteMapping("/delete/{id}/{name}")
    public RespBean deleteById(@PathVariable("id") String result, @PathVariable("name") String name) {

        return RespBean.success("成功删除" + result + "," + name + "！");
    }

    @PostMapping("/insert")
    public RespBean insertUser(@RequestBody UserInfo userInfo) {

        System.out.println(userInfo);
        return RespBean.success("新增成功！");
    }

    @PostMapping("/synData")
    public RespBean synData(@RequestBody Map<String, Object> data) {

        // map的遍历输出
        // 1. keySet 获取key集合
        System.out.println("keySet 循环");
        for (String key : data.keySet()) {

            System.out.println(key + ":" + data.get(key));
        }
        // 2. entrySet 获取每一个键值对，构成一个键值对的集合，Entry
        System.out.println("entrySet 循环");
        for (Map.Entry<String, Object> entry : data.entrySet()) {

            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        // 3. 使用函数式方法，jdk8之后才有的功能
        System.out.println("函数式循环");
        Consumer<Map.Entry<String, Object>> consumer = new Consumer<Map.Entry<String, Object>>() {

            @Override
            public void accept(Map.Entry<String, Object> stringObjectEntry) {
                System.out.println(stringObjectEntry.getKey() + ":" + stringObjectEntry.getValue());
            }
        };
        data.entrySet().forEach(consumer);
        // 4. lambda表达式来定义循环的动作
        System.out.println("lambda表达式定义循环...");
        data.entrySet().forEach(x -> System.out.println(x.getKey() + ":" + x.getValue()));


        return RespBean.success("请求成功！", data);
    }

    @PostMapping("/synData2")
    public RespBean synData2(@RequestBody JSONObject data) {

        UserInfo userInfo = data.getObject("data", UserInfo.class);
        String email = data.getString("email");
        JSONArray classroom = data.getJSONArray("classroom");
        List<Integer> classrooms = classroom.toJavaList(Integer.class);

        System.out.println(userInfo);
        System.out.println(email);
        System.out.println(classrooms);

        return RespBean.success("请求成功");
    }

    @GetMapping("/fromHead")
    public RespBean takeDataFromHead(@RequestHeader("User-Agent") String userAgent, @RequestHeader("Auth") String auth) {

        return RespBean.success("获取到User-Agent:"+ userAgent + ", 获取到Auth:" + auth);
    }

    @PostMapping("/insert2")
    public RespBean insertUser(HttpServletRequest request) throws IOException {

        // 获取请求头的数据
        String header = request.getHeader("Content-Type");
        System.out.println(header);

        // 获取请求参数 ？key=value @RequestParam
        String value = request.getParameter("key");
        System.out.println(value);

        // 地址中的变量
        String requestURI = request.getRequestURI();
        System.out.println(requestURI);

        // @RequestBody
        // 1. 将客户端传递的请求体中的数据读取出来,解析为字符串String
        // 2. 字符串再通过ObjectMapper转换为UserInfo对象
        // 3. 将对象响应出去的时候，框架又会将java对象转换为json字符串 -> byte[] 字节流 -> 电信号 -> 传递给客户端 -> byte[] -> string -> 转换为js对象
        // 序列化：将java对象转换为便于网络传输的、或者用于保存文件的字节流
        // 反序列化：将便于网络传输的字节流转换为java对象
        // 从客户端读取数据的总长度
        int contentLength = request.getContentLength();
        if (contentLength < 0) {
            throw new RuntimeException("数据为空！");
        }
        byte[] buffer = new byte[contentLength];

        for (int i = 0; i < contentLength;) {

            int readLength = request.getInputStream().read(buffer, i, contentLength - i);
            if (readLength == -1) {
                break;
            }
            i += readLength;
        }
        String json = new String(buffer, "UTF-8");
        System.out.println(json);

        ObjectMapper objectMapper = new ObjectMapper();
        UserInfo userInfo = objectMapper.readValue(json, UserInfo.class);

        System.out.println(userInfo);

        return RespBean.success("新增成功！", userInfo);
    }

    @GetMapping("/exception")
    public RespBean exceptionTest() {

        int i = 1/0;
        return RespBean.success("请求成功！");
    }

    @PostMapping("/login")
    public RespBean testLogin(@RequestBody JSONObject auth) {

        String username = auth.getString("username");
        String password = auth.getString("password");

        if (!"admin".equals(username) || !"123456".equals(password)) {

            throw new AuthenticationException("用户名或者密码不正确！");
        }

        return RespBean.success("登录成功！");
    }
}
