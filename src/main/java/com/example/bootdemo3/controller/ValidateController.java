package com.example.bootdemo3.controller;


import com.example.bootdemo3.domain.vo.RespBean;
import com.example.bootdemo3.validate.annotation.MyBetween;
import com.example.bootdemo3.validate.annotation.MyRequired;
import com.example.bootdemo3.validate.annotation.MyValidated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class ValidateController {

    @GetMapping("/1")
    @MyValidated
    public RespBean validate1(@RequestParam("name") @MyRequired String name,
                              @RequestParam("age") @MyRequired @MyBetween(min = 18, max = 99) Integer age) {

        return RespBean.success("name以及age校验通过！");
    }
}
