package com.example.bootdemo3.service;

import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.domain.vo.PageResult;

import java.util.List;

/**
 * 增删改查
 * get
 * add
 * edit
 * remove
 */
public interface ISysUserService {

    SysUser getByAuth(SysUser sysUser);

    List<SysUser> getAll();

    int addSysUser(SysUser sysUser);

    int editSysUser(SysUser sysUser);

    int removeById(int id);

    List<SysUser> getByConditions(SysUser sysUser);

    int removeByIds(Integer[] userIds);

    PageResult<SysUser> getAllPage(Integer currentPage, Integer size);

    int addBatch(SysUser[] sysUsers);

    List<SysUser> findAllWithRoleAndPerm();
}
