package com.example.bootdemo3.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bootdemo3.domain.SysPermission;
import com.example.bootdemo3.mapper.SysPermissionMapper;
import com.example.bootdemo3.service.ISysPermissionService;
import org.springframework.stereotype.Service;

@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {
}
