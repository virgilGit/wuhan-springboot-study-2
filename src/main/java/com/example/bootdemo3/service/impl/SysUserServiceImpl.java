package com.example.bootdemo3.service.impl;

import com.example.bootdemo3.domain.SysUser;
import com.example.bootdemo3.domain.vo.PageResult;
import com.example.bootdemo3.mapper.SysUserMapper;
import com.example.bootdemo3.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Service修饰类，将当前对象注册到spring容器中
 * 能够通过spring进行不同对象、组件的统一调配
 */
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public SysUser getByAuth(SysUser sysUser) {

        return sysUserMapper.findByUsernameAndPassword(sysUser.getUsername(), sysUser.getPassword());
    }

    @Override
    public List<SysUser> getAll() {
        return sysUserMapper.findAll();
    }

    @Override
    public int addSysUser(SysUser sysUser) {
        return sysUserMapper.insertSysUser(sysUser);
    }

    @Override
    public int editSysUser(SysUser sysUser) {
        return sysUserMapper.updateSysUser(sysUser);
    }

    @Override
    public int removeById(int id) {
        return sysUserMapper.deleteById(id);
    }

    @Override
    public List<SysUser> getByConditions(SysUser sysUser) {
        return sysUserMapper.findByConditions(sysUser);
    }

    @Override
    public int removeByIds(Integer[] userIds) {

        return sysUserMapper.deleteByIds(userIds);
    }

    @Override
    public PageResult<SysUser> getAllPage(Integer currentPage, Integer size) {

        // 总数
        int total = sysUserMapper.findAllCount();
        // 总页数：ceil(total / size) 7/4 = 2.0 -> 2
        // 向上转型，将精度高的转换为精度低 double -> int 需要手动转型
        // 会出现精度损失的情况
        int totalPage = (int) Math.ceil(total * 1.0 / size);

        List<SysUser> list = sysUserMapper.findAllPage(
                (currentPage - 1) * size,
                size
        );

        return new PageResult<SysUser>()
                .setCurrentPage(currentPage)
                .setSize(size)
                .setTotal(total)
                .setTotalPage(totalPage)
                .setData(list);
    }

    @Override
    public int addBatch(SysUser[] sysUsers) {
        return sysUserMapper.insertBatch(sysUsers);
    }

    @Override
    public List<SysUser> findAllWithRoleAndPerm() {
        return sysUserMapper.findAllWithRoles2();
    }
}
