package com.example.bootdemo3.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bootdemo3.domain.SysPermission;

public interface ISysPermissionService extends IService<SysPermission> {

}
