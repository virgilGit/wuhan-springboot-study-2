package com.example.bootdemo3.exception;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String msg) {

        super(msg);
    }
}
