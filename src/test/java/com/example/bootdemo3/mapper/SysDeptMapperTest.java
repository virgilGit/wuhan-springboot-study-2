package com.example.bootdemo3.mapper;

import com.example.bootdemo3.domain.SysDept;
import com.example.bootdemo3.domain.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class SysDeptMapperTest {

    @Autowired
    private SysDeptMapper deptMapper;

    @Test
    void findAllWithUser() {

        List<SysDept> list = deptMapper.findAllWithUser();
        list.forEach(System.out::println);
    }

    @Test
    void findAllWithUser2() {

        List<SysDept> list = deptMapper.findAllWithUser2();
        list.forEach(System.out::println);
    }
}