package com.example.bootdemo3.mapper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.bootdemo3.domain.SysMenu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class SysMenuMapperTest {

    @Autowired
    private SysMenuMapper menuMapper;

    @Test
    void findByParentId() {

        List<SysMenu> menus = menuMapper.findByParentId(0);

        String s = JSON.toJSONString(menus);
        System.out.println(s);
    }
}