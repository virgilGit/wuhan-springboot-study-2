package com.example.bootdemo3.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.bootdemo3.domain.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * SpirngBootTest修饰类，表示将该类单例化（new 一个对象）这个对象交给spring来管理
 * 标记当前的对象用来进行单元测试的
 *
 * 测试git
 * 由spring管理的所有的实例（对象）都可以统一仅由spring进行调配
 */
@SpringBootTest
public class SysUserMapperTest {

    /**
     * SysUserMapper是交给springboot统一管理的，
     * 该对象就可以注入到SysUserMapperTest对象引用中
     */
    @Autowired
    private SysUserMapper sysUserMapper;

    @Test
    public void findAll() {

        List<SysUser> sysUsers = sysUserMapper.findAll();

        sysUsers.forEach(System.out::println);
    }

    @Test
    public void insertSysUser() {

        // 查看某个方法的参数列表，可以在括号中Ctrl + P
        int result = sysUserMapper.insertSysUser(
                new SysUser(null, "Scott", "123456", "23222@qq.com"));
        System.out.println("成功新增了" + result + "条");
    }

    @Test
    void insertSysUser2() {

        Map<String, Object> params = new HashMap<>();
        params.put("username", "snow");
        params.put("password", "123456");
        params.put("email", "1234@qq.com");
        int result = sysUserMapper.insertSysUser2(params);

        System.out.println("成功新增了" + result + "条");
    }

    @Test
    public void updateSysUser() {

        int result = sysUserMapper.updateSysUser(
                new SysUser(4, "Snow", "123456", "111@163.com"));
        System.out.println("成功修改了" + result + "条");
    }

    @Test
    void deleteById() {

        int result = sysUserMapper.deleteById(4);
        System.out.println("成功删除了" + result + "条！");
    }

    @Test
    void findByUsernameAndPassword() {

        SysUser user = sysUserMapper.findByUsernameAndPassword("Scott", "123456");
        System.out.println(user);
    }

    @Test
    void findAllCount() {
        int allCount = sysUserMapper.findAllCount();
        System.out.println(allCount);
    }

    @Test
    void findAllCountByGender() {

        int count = sysUserMapper.findAllCountByGender(1);
        System.out.println(count);
    }

    @Test
    void findGenderCount() {

        List<Map<String, Object>> list = sysUserMapper.findGenderCount();

        // 输出的数据：
        /**
         * 作业1：
         * 原来：
         * total:1
         * gender:女性
         * total:2
         * gender:男性
         *
         * 要求：
         * total:1；gender:女性
         * total:2；gender:男性
         *
         * (x) -> {
         *     x.forEach()...
         * }
         */
        list.forEach(x -> x.forEach((a,b) -> {

            System.out.println(a + ":" + b);
        }));
    }

    @Test
    void findLikeUsername() {

        List<SysUser> users = sysUserMapper.findLikeUsername("%小%");

        users.forEach(System.out::println);
    }

    @Test
    void findLikeUsername2() {

        List<SysUser> users = sysUserMapper.findLikeUsername2("小' or 1=1 -- ");
        users.forEach(System.out::println);
    }


    @Test
    void findLikeUsername3() {

        List<SysUser> users = sysUserMapper.findLikeUsername3("小) or 1=1 -- ");
        users.forEach(System.out::println);
    }

    @Test
    void deleteByIds() {

        Integer[] ids = {6,7,8};
        int result = sysUserMapper.deleteByIds(ids);

        System.out.println("总共删除了" + result + "条");
    }

    @Test
    void findAllWithAccount() {

        List<SysUser> users = sysUserMapper.findAllWithAccount();
        users.forEach(System.out::println);
    }

    @Test
    void findAllWithRoles() {

        List<SysUser> users = sysUserMapper.findAllWithRoles();
        users.forEach(System.out::println);
    }

    @Test
    void findAllWithAccount2() {

        List<SysUser> sysUsers = sysUserMapper.findAllWithAccount2();

        sysUsers.forEach(System.out::println);
    }

    @Test
    void findAllWithRoles2() {

        List<SysUser> users = sysUserMapper.findAllWithRoles2();
        users.forEach(System.out::println);
    }

    @Test
    public void insert() {

        int result = sysUserMapper.insert(new SysUser(null, "小红3", "123456", "888@163.com").setGender(1));
        System.out.println(result);
    }

    @Test
    public void select() {

        List<SysUser> sysUsers = sysUserMapper.selectList(
                new QueryWrapper<SysUser>()
                        .like("username", "小")
                        .eq("gender", 1)
        );
        for (SysUser sysUser : sysUsers) {
            System.out.println(sysUser);
        }
    }
}