package com.example.bootdemo3;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class JwtTest {

    @Test
    public void generateToken() {

        long exp = System.currentTimeMillis() + 60 * 1000;
        JwtBuilder jwtBuilder = Jwts.builder()
                // 设置jwt的唯一标识 实际上实在荷载payload {jti: xxxxxx}
                .setId("xxxxx")
                // 设置jwt的用户
                .setSubject("Lucy")
                .setIssuedAt(new Date())
                .claim("roles", "admin,public,tester")
                // salt 盐值
                .signWith(SignatureAlgorithm.HS256, "xxx".getBytes())
                .setExpiration(new Date(exp));

        String token = jwtBuilder.compact();
        System.out.println(token);
    }

    @Test
    public void parseToken() {


        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ4eHh4eCIsInN1YiI6Ikx1Y3kiLCJpYXQiOjE2NTkxNjE2MjYsInJvbGVzIjoiYWRtaW4scHVibGljLHRlc3RlciIsImV4cCI6MTY1OTE2MTY4NX0.48E_LIAXCfl0FmlvKJ8kNHbXrDd96nPAapxlhkElnvs";

        Claims body = Jwts.parser()
                .setSigningKey("xxx".getBytes())
                .parseClaimsJws(token)
                .getBody();

        System.out.println(body);
        String id = body.getId();
        System.out.println("id:" + id);
        String subject = body.getSubject();
        System.out.println("subject" + subject);
        Object roles = body.get("roles");
        System.out.println("roles:" + roles);

        Date expiration = body.getExpiration();
        System.out.println(expiration);

    }
}
