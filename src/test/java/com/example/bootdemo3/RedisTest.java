package com.example.bootdemo3;

import com.example.bootdemo3.domain.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void initConn() {

        /**
         * RedisTempalte有序列化的功能
         * 如果在RedisTempalte中不去设置key和value，默认就是Object
         * Object在java中随便用，但是在redis中opsForValue只支持字符串，redis会自动按照约定好的序列化方式
         * 将Java中的object对象转换为便于网络传输或者保存文件的字节码
         *
         * key -> String
         * value -> Map (Object)
         * 如果单单是java读取倒也无所谓，但是我们的redis一般作为一个公共的中间组件来使用
         * 连接redis的可不仅仅只有java写的后台代码，php .net python
         * kay -> String
         * value -> json json是一个标准，连接了后台和前端
         * 接下来，我们配置value序列化的方式，将java对象 -> json字符串 -> redis
         */
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.set("test1", "success1!");
        Object test = valueOperations.get("test");
        System.out.println(test);
    }

    @Test
    public void test2() {

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();

        String s = UUID.randomUUID().toString();
        System.out.println(s);
        String replace = s.replace("-", "");
        operations.set("MYSESSIONID_" + replace,
                new SysUser(1, "小红", "123456", "xxxx@qq.com"),
                10,
                TimeUnit.SECONDS
        );

    }

    @Test
    public void test3() {

        ValueOperations<String, Object> operations = redisTemplate.opsForValue();

        SysUser user = (SysUser) operations.get("MYSESSIONID_" + "19d644a75c58466d988856baaba746ec");

        System.out.println(user);
    }
}
